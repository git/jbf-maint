CONFIGURATION FILE
------------------

The git configuration file contains a number of variables that affect
the git command's behavior. `.git/config` file for each repository
is used to store the information for that repository, and
`$HOME/.gitconfig` is used to store per user information to give
fallback values for `.git/config` file. The file `/etc/gitconfig`
can be used to store system-wide defaults.

They can be used by both the git plumbing
and the porcelains. The variables are divided into sections, where
in the fully qualified variable name the variable itself is the last
dot-separated segment and the section name is everything before the last
dot. The variable names are case-insensitive and only alphanumeric
characters are allowed. Some variables may appear multiple times.

Syntax
~~~~~~

The syntax is fairly flexible and permissive; whitespaces are mostly
ignored.  The '#' and ';' characters begin comments to the end of line,
blank lines are ignored.

The file consists of sections and variables.  A section begins with
the name of the section in square brackets and continues until the next
section begins.  Section names are not case sensitive.  Only alphanumeric
characters, '`-`' and '`.`' are allowed in section names.  Each variable
must belong to some section, which means that there must be section
header before first setting of a variable.

Sections can be further divided into subsections.  To begin a subsection
put its name in double quotes, separated by space from the section name,
in the section header, like in example below:

--------
	[section "subsection"]

--------

Subsection names can contain any characters except newline (doublequote
'`"`' and backslash have to be escaped as '`\"`' and '`\\`',
respectively) and are case sensitive.  Section header cannot span multiple
lines.  Variables may belong directly to a section or to a given subsection.
You can have `[section]` if you have `[section "subsection"]`, but you
don't need to.

There is also (case insensitive) alternative `[section.subsection]` syntax.
In this syntax subsection names follow the same restrictions as for section
name.

All the other lines are recognized as setting variables, in the form
'name = value'.  If there is no equal sign on the line, the entire line
is taken as 'name' and the variable is recognized as boolean "true".
The variable names are case-insensitive and only alphanumeric
characters and '`-`' are allowed.  There can be more than one value
for a given variable; we say then that variable is multivalued.

Leading and trailing whitespace in a variable value is discarded.
Internal whitespace within a variable value is retained verbatim.

The values following the equals sign in variable assign are all either
a string, an integer, or a boolean.  Boolean values may be given as yes/no,
0/1 or true/false.  Case is not significant in boolean values, when
converting value to the canonical form using '--bool' type specifier;
`git-config` will ensure that the output is "true" or "false".

String values may be entirely or partially enclosed in double quotes.
You need to enclose variable value in double quotes if you want to
preserve leading or trailing whitespace, or if variable value contains
beginning of comment characters (if it contains '#' or ';').
Double quote '`"`' and backslash '`\`' characters in variable value must
be escaped: use '`\"`' for '`"`' and '`\\`' for '`\`'.

The following escape sequences (beside '`\"`' and '`\\`') are recognized:
'`\n`' for newline character (NL), '`\t`' for horizontal tabulation (HT, TAB)
and '`\b`' for backspace (BS).  No other char escape sequence, nor octal
char sequences are valid.

Variable value ending in a '`\`' is continued on the next line in the
customary UNIX fashion.

Some variables may require special value format.

Example
~~~~~~~

	# Core variables
	[core]
		; Don't trust file modes
		filemode = false

	# Our diff algorithm
	[diff]
		external = "/usr/local/bin/gnu-diff -u"
		renames = true

	[branch "devel"]
		remote = origin
		merge = refs/heads/devel

	# Proxy settings
	[core]
		gitProxy="ssh" for "kernel.org"
		gitProxy=default-proxy ; for the rest

Variables
~~~~~~~~~

Note that this list is non-comprehensive and not necessarily complete.
For command-specific variables, you will find a more detailed description
in the appropriate manual page. You will find a description of non-core
porcelain configuration variables in the respective porcelain documentation.

core.fileMode::
	If false, the executable bit differences between the index and
	the working copy are ignored; useful on broken filesystems like FAT.
	See gitlink:git-update-index[1]. True by default.

core.quotepath::
	The commands that output paths (e.g. `ls-files`,
	`diff`), when not given the `-z` option, will quote
	"unusual" characters in the pathname by enclosing the
	pathname in a double-quote pair and with backslashes the
	same way strings in C source code are quoted.  If this
	variable is set to false, the bytes higher than 0x80 are
	not quoted but output as verbatim.  Note that double
	quote, backslash and control characters are always
	quoted without `-z` regardless of the setting of this
	variable.

core.autocrlf::
	If true, makes git convert `CRLF` at the end of lines in text files to
	`LF` when reading from the filesystem, and convert in reverse when
	writing to the filesystem.  The variable can be set to
	'input', in which case the conversion happens only while
	reading from the filesystem but files are written out with
	`LF` at the end of lines.  Currently, which paths to consider
	"text" (i.e. be subjected to the autocrlf mechanism) is
	decided purely based on the contents.

core.symlinks::
	If false, symbolic links are checked out as small plain files that
	contain the link text. gitlink:git-update-index[1] and
	gitlink:git-add[1] will not change the recorded type to regular
	file. Useful on filesystems like FAT that do not support
	symbolic links. True by default.

core.gitProxy::
	A "proxy command" to execute (as 'command host port') instead
	of establishing direct connection to the remote server when
	using the git protocol for fetching. If the variable value is
	in the "COMMAND for DOMAIN" format, the command is applied only
	on hostnames ending with the specified domain string. This variable
	may be set multiple times and is matched in the given order;
	the first match wins.
+
Can be overridden by the 'GIT_PROXY_COMMAND' environment variable
(which always applies universally, without the special "for"
handling).

core.ignoreStat::
	The working copy files are assumed to stay unchanged until you
	mark them otherwise manually - Git will not detect the file changes
	by lstat() calls. This is useful on systems where those are very
	slow, such as Microsoft Windows.  See gitlink:git-update-index[1].
	False by default.

core.preferSymlinkRefs::
	Instead of the default "symref" format for HEAD
	and other symbolic reference files, use symbolic links.
	This is sometimes needed to work with old scripts that
	expect HEAD to be a symbolic link.

core.bare::
	If true this repository is assumed to be 'bare' and has no
	working directory associated with it.  If this is the case a
	number of commands that require a working directory will be
	disabled, such as gitlink:git-add[1] or gitlink:git-merge[1].
+
This setting is automatically guessed by gitlink:git-clone[1] or
gitlink:git-init[1] when the repository was created.  By default a
repository that ends in "/.git" is assumed to be not bare (bare =
false), while all other repositories are assumed to be bare (bare
= true).

core.worktree::
	Set the path to the working tree.  The value will not be
	used in combination with repositories found automatically in
	a .git directory (i.e. $GIT_DIR is not set).
	This can be overridden by the GIT_WORK_TREE environment
	variable and the '--work-tree' command line option.

core.logAllRefUpdates::
	Enable the reflog. Updates to a ref <ref> is logged to the file
	"$GIT_DIR/logs/<ref>", by appending the new and old
	SHA1, the date/time and the reason of the update, but
	only when the file exists.  If this configuration
	variable is set to true, missing "$GIT_DIR/logs/<ref>"
	file is automatically created for branch heads.
+
This information can be used to determine what commit
was the tip of a branch "2 days ago".
+
This value is true by default in a repository that has
a working directory associated with it, and false by
default in a bare repository.

core.repositoryFormatVersion::
	Internal variable identifying the repository format and layout
	version.

core.sharedRepository::
	When 'group' (or 'true'), the repository is made shareable between
	several users in a group (making sure all the files and objects are
	group-writable). When 'all' (or 'world' or 'everybody'), the
	repository will be readable by all users, additionally to being
	group-shareable. When 'umask' (or 'false'), git will use permissions
	reported by umask(2). See gitlink:git-init[1]. False by default.

core.warnAmbiguousRefs::
	If true, git will warn you if the ref name you passed it is ambiguous
	and might match multiple refs in the .git/refs/ tree. True by default.

core.compression::
	An integer -1..9, indicating a default compression level.
	-1 is the zlib default. 0 means no compression,
	and 1..9 are various speed/size tradeoffs, 9 being slowest.
	If set, this provides a default to other compression variables,
	such as 'core.loosecompression' and 'pack.compression'.

core.loosecompression::
	An integer -1..9, indicating the compression level for objects that
	are not in a pack file. -1 is the zlib default. 0 means no
	compression, and 1..9 are various speed/size tradeoffs, 9 being
	slowest.  If not set,  defaults to core.compression.  If that is
	not set,  defaults to 1 (best speed).

core.packedGitWindowSize::
	Number of bytes of a pack file to map into memory in a
	single mapping operation.  Larger window sizes may allow
	your system to process a smaller number of large pack files
	more quickly.  Smaller window sizes will negatively affect
	performance due to increased calls to the operating system's
	memory manager, but may improve performance when accessing
	a large number of large pack files.
+
Default is 1 MiB if NO_MMAP was set at compile time, otherwise 32
MiB on 32 bit platforms and 1 GiB on 64 bit platforms.  This should
be reasonable for all users/operating systems.  You probably do
not need to adjust this value.
+
Common unit suffixes of 'k', 'm', or 'g' are supported.

core.packedGitLimit::
	Maximum number of bytes to map simultaneously into memory
	from pack files.  If Git needs to access more than this many
	bytes at once to complete an operation it will unmap existing
	regions to reclaim virtual address space within the process.
+
Default is 256 MiB on 32 bit platforms and 8 GiB on 64 bit platforms.
This should be reasonable for all users/operating systems, except on
the largest projects.  You probably do not need to adjust this value.
+
Common unit suffixes of 'k', 'm', or 'g' are supported.

core.deltaBaseCacheLimit::
	Maximum number of bytes to reserve for caching base objects
	that multiple deltafied objects reference.  By storing the
	entire decompressed base objects in a cache Git is able
	to avoid unpacking and decompressing frequently used base
	objects multiple times.
+
Default is 16 MiB on all platforms.  This should be reasonable
for all users/operating systems, except on the largest projects.
You probably do not need to adjust this value.
+
Common unit suffixes of 'k', 'm', or 'g' are supported.

core.excludesfile::
	In addition to '.gitignore' (per-directory) and
	'.git/info/exclude', git looks into this file for patterns
	of files which are not meant to be tracked.  See
	gitlink:gitignore[5].

core.editor::
	Commands such as `commit` and `tag` that lets you edit
	messages by launching an editor uses the value of this
	variable when it is set, and the environment variable
	`GIT_EDITOR` is not set.  The order of preference is
	`GIT_EDITOR` environment, `core.editor`, `VISUAL` and
	`EDITOR` environment variables and then finally `vi`.

core.pager::
	The command that git will use to paginate output.  Can be overridden
	with the `GIT_PAGER` environment variable.

core.whitespace::
	A comma separated list of common whitespace problems to
	notice.  `git diff` will use `color.diff.whitespace` to
	highlight them, and `git apply --whitespace=error` will
	consider them as errors:
+
* `trailing-space` treats trailing whitespaces at the end of the line
  as an error (enabled by default).
* `space-before-tab` treats a space character that appears immediately
  before a tab character in the initial indent part of the line as an
  error (enabled by default).
* `indent-with-non-tab` treats a line that is indented with 8 or more
  space characters as an error (not enabled by default).

alias.*::
	Command aliases for the gitlink:git[1] command wrapper - e.g.
	after defining "alias.last = cat-file commit HEAD", the invocation
	"git last" is equivalent to "git cat-file commit HEAD". To avoid
	confusion and troubles with script usage, aliases that
	hide existing git commands are ignored. Arguments are split by
	spaces, the usual shell quoting and escaping is supported.
	quote pair and a backslash can be used to quote them.
+
If the alias expansion is prefixed with an exclamation point,
it will be treated as a shell command.  For example, defining
"alias.new = !gitk --all --not ORIG_HEAD", the invocation
"git new" is equivalent to running the shell command
"gitk --all --not ORIG_HEAD".

apply.whitespace::
	Tells `git-apply` how to handle whitespaces, in the same way
	as the '--whitespace' option. See gitlink:git-apply[1].

branch.autosetupmerge::
	Tells `git-branch` and `git-checkout` to setup new branches
	so that gitlink:git-pull[1] will appropriately merge from that
	remote branch.  Note that even if this option is not set,
	this behavior can be chosen per-branch using the `--track`
	and `--no-track` options.  This option defaults to false.

branch.<name>.remote::
	When in branch <name>, it tells `git fetch` which remote to fetch.
	If this option is not given, `git fetch` defaults to remote "origin".

branch.<name>.merge::
	When in branch <name>, it tells `git fetch` the default
	refspec to be marked for merging in FETCH_HEAD. The value is
	handled like the remote part of a refspec, and must match a
	ref which is fetched from the remote given by
	"branch.<name>.remote".
	The merge information is used by `git pull` (which at first calls
	`git fetch`) to lookup the default branch for merging. Without
	this option, `git pull` defaults to merge the first refspec fetched.
	Specify multiple values to get an octopus merge.
	If you wish to setup `git pull` so that it merges into <name> from
	another branch in the local repository, you can point
	branch.<name>.merge to the desired branch, and use the special setting
	`.` (a period) for branch.<name>.remote.

branch.<name>.mergeoptions::
	Sets default options for merging into branch <name>. The syntax and
	supported options are equal to that of gitlink:git-merge[1], but
	option values containing whitespace characters are currently not
	supported.

branch.<name>.rebase::
	When true, rebase the branch <name> on top of the fetched branch,
	instead of merging the default branch from the default remote.
	*NOTE*: this is a possibly dangerous operation; do *not* use
	it unless you understand the implications (see gitlink:git-rebase[1]
	for details).

clean.requireForce::
	A boolean to make git-clean do nothing unless given -f
	or -n.   Defaults to true.

color.branch::
	A boolean to enable/disable color in the output of
	gitlink:git-branch[1]. May be set to `always`,
	`false` (or `never`) or `auto` (or `true`), in which case colors are used
	only when the output is to a terminal. Defaults to false.

color.branch.<slot>::
	Use customized color for branch coloration. `<slot>` is one of
	`current` (the current branch), `local` (a local branch),
	`remote` (a tracking branch in refs/remotes/), `plain` (other
	refs).
+
The value for these configuration variables is a list of colors (at most
two) and attributes (at most one), separated by spaces.  The colors
accepted are `normal`, `black`, `red`, `green`, `yellow`, `blue`,
`magenta`, `cyan` and `white`; the attributes are `bold`, `dim`, `ul`,
`blink` and `reverse`.  The first color given is the foreground; the
second is the background.  The position of the attribute, if any,
doesn't matter.

color.diff::
	When set to `always`, always use colors in patch.
	When false (or `never`), never.  When set to `true` or `auto`, use
	colors only when the output is to the terminal. Defaults to false.

color.diff.<slot>::
	Use customized color for diff colorization.  `<slot>` specifies
	which part of the patch to use the specified color, and is one
	of `plain` (context text), `meta` (metainformation), `frag`
	(hunk header), `old` (removed lines), `new` (added lines),
	`commit` (commit headers), or `whitespace` (highlighting
	whitespace errors). The values of these variables may be specified as
	in color.branch.<slot>.

color.interactive::
	When set to `always`, always use colors in `git add --interactive`.
	When false (or `never`), never.  When set to `true` or `auto`, use
	colors only when the output is to the terminal. Defaults to false.

color.interactive.<slot>::
	Use customized color for `git add --interactive`
	output. `<slot>` may be `prompt`, `header`, or `help`, for
	three distinct types of normal output from interactive
	programs.  The values of these variables may be specified as
	in color.branch.<slot>.

color.pager::
	A boolean to enable/disable colored output when the pager is in
	use (default is true).

color.status::
	A boolean to enable/disable color in the output of
	gitlink:git-status[1]. May be set to `always`,
	`false` (or `never`) or `auto` (or `true`), in which case colors are used
	only when the output is to a terminal. Defaults to false.

color.status.<slot>::
	Use customized color for status colorization. `<slot>` is
	one of `header` (the header text of the status message),
	`added` or `updated` (files which are added but not committed),
	`changed` (files which are changed but not added in the index),
	or `untracked` (files which are not tracked by git). The values of
	these variables may be specified as in color.branch.<slot>.

commit.template::
	Specify a file to use as the template for new commit messages.

diff.autorefreshindex::
	When using `git diff` to compare with work tree
	files, do not consider stat-only change as changed.
	Instead, silently run `git update-index --refresh` to
	update the cached stat information for paths whose
	contents in the work tree match the contents in the
	index.  This option defaults to true.  Note that this
	affects only `git diff` Porcelain, and not lower level
	`diff` commands, such as `git diff-files`.

diff.renameLimit::
	The number of files to consider when performing the copy/rename
	detection; equivalent to the git diff option '-l'.

diff.renames::
	Tells git to detect renames.  If set to any boolean value, it
	will enable basic rename detection.  If set to "copies" or
	"copy", it will detect copies, as well.

fetch.unpackLimit::
	If the number of objects fetched over the git native
	transfer is below this
	limit, then the objects will be unpacked into loose object
	files. However if the number of received objects equals or
	exceeds this limit then the received pack will be stored as
	a pack, after adding any missing delta bases.  Storing the
	pack from a push can make the push operation complete faster,
	especially on slow filesystems.

format.numbered::
	A boolean which can enable sequence numbers in patch subjects.
	Seting this option to "auto" will enable it only if there is
	more than one patch.  See --numbered option in
	gitlink:git-format-patch[1].

format.headers::
	Additional email headers to include in a patch to be submitted
	by mail.  See gitlink:git-format-patch[1].

format.suffix::
	The default for format-patch is to output files with the suffix
	`.patch`. Use this variable to change that suffix (make sure to
	include the dot if you want it).

gc.aggressiveWindow::
	The window size parameter used in the delta compression
	algorithm used by 'git gc --aggressive'.  This defaults
	to 10.

gc.auto::
	When there are approximately more than this many loose
	objects in the repository, `git gc --auto` will pack them.
	Some Porcelain commands use this command to perform a
	light-weight garbage collection from time to time.  Setting
	this to 0 disables it.

gc.autopacklimit::
	When there are more than this many packs that are not
	marked with `*.keep` file in the repository, `git gc
	--auto` consolidates them into one larger pack.  Setting
	this to 0 disables this.

gc.packrefs::
	`git gc` does not run `git pack-refs` in a bare repository by
	default so that older dumb-transport clients can still fetch
	from the repository.  Setting this to `true` lets `git
	gc` to run `git pack-refs`.  Setting this to `false` tells
	`git gc` never to run `git pack-refs`. The default setting is
	`notbare`. Enable it only when you know you do not have to
	support such clients.  The default setting will change to `true`
	at some stage, and setting this to `false` will continue to
	prevent `git pack-refs` from being run from `git gc`.

gc.reflogexpire::
	`git reflog expire` removes reflog entries older than
	this time; defaults to 90 days.

gc.reflogexpireunreachable::
	`git reflog expire` removes reflog entries older than
	this time and are not reachable from the current tip;
	defaults to 30 days.

gc.rerereresolved::
	Records of conflicted merge you resolved earlier are
	kept for this many days when `git rerere gc` is run.
	The default is 60 days.  See gitlink:git-rerere[1].

gc.rerereunresolved::
	Records of conflicted merge you have not resolved are
	kept for this many days when `git rerere gc` is run.
	The default is 15 days.  See gitlink:git-rerere[1].

rerere.enabled::
	Activate recording of resolved conflicts, so that identical
	conflict hunks can be resolved automatically, should they
	be encountered again.  gitlink:git-rerere[1] command is by
	default enabled, but can be disabled by setting this option to
	false.

gitcvs.enabled::
	Whether the CVS server interface is enabled for this repository.
	See gitlink:git-cvsserver[1].

gitcvs.logfile::
	Path to a log file where the CVS server interface well... logs
	various stuff. See gitlink:git-cvsserver[1].

gitcvs.allbinary::
	If true, all files are sent to the client in mode '-kb'. This
	causes the client to treat all files as binary files which suppresses
	any newline munging it otherwise might do. A work-around for the
	fact that there is no way yet to set single files to mode '-kb'.

gitcvs.dbname::
	Database used by git-cvsserver to cache revision information
	derived from the git repository. The exact meaning depends on the
	used database driver, for SQLite (which is the default driver) this
	is a filename. Supports variable substitution (see
	gitlink:git-cvsserver[1] for details). May not contain semicolons (`;`).
	Default: '%Ggitcvs.%m.sqlite'

gitcvs.dbdriver::
	Used Perl DBI driver. You can specify any available driver
        for this here, but it might not work. git-cvsserver is tested
	with 'DBD::SQLite', reported to work with 'DBD::Pg', and
	reported *not* to work with 'DBD::mysql'. Experimental feature.
	May not contain double colons (`:`). Default: 'SQLite'.
	See gitlink:git-cvsserver[1].

gitcvs.dbuser, gitcvs.dbpass::
	Database user and password. Only useful if setting 'gitcvs.dbdriver',
	since SQLite has no concept of database users and/or passwords.
	'gitcvs.dbuser' supports variable substitution (see
	gitlink:git-cvsserver[1] for details).

All gitcvs variables except for 'gitcvs.allbinary' can also be
specified as 'gitcvs.<access_method>.<varname>' (where 'access_method'
is one of "ext" and "pserver") to make them apply only for the given
access method.

http.proxy::
	Override the HTTP proxy, normally configured using the 'http_proxy'
	environment variable (see gitlink:curl[1]).  This can be overridden
	on a per-remote basis; see remote.<name>.proxy

http.sslVerify::
	Whether to verify the SSL certificate when fetching or pushing
	over HTTPS. Can be overridden by the 'GIT_SSL_NO_VERIFY' environment
	variable.

http.sslCert::
	File containing the SSL certificate when fetching or pushing
	over HTTPS. Can be overridden by the 'GIT_SSL_CERT' environment
	variable.

http.sslKey::
	File containing the SSL private key when fetching or pushing
	over HTTPS. Can be overridden by the 'GIT_SSL_KEY' environment
	variable.

http.sslCAInfo::
	File containing the certificates to verify the peer with when
	fetching or pushing over HTTPS. Can be overridden by the
	'GIT_SSL_CAINFO' environment variable.

http.sslCAPath::
	Path containing files with the CA certificates to verify the peer
	with when fetching or pushing over HTTPS. Can be overridden
	by the 'GIT_SSL_CAPATH' environment variable.

http.maxRequests::
	How many HTTP requests to launch in parallel. Can be overridden
	by the 'GIT_HTTP_MAX_REQUESTS' environment variable. Default is 5.

http.lowSpeedLimit, http.lowSpeedTime::
	If the HTTP transfer speed is less than 'http.lowSpeedLimit'
	for longer than 'http.lowSpeedTime' seconds, the transfer is aborted.
	Can be overridden by the 'GIT_HTTP_LOW_SPEED_LIMIT' and
	'GIT_HTTP_LOW_SPEED_TIME' environment variables.

http.noEPSV::
	A boolean which disables using of EPSV ftp command by curl.
	This can helpful with some "poor" ftp servers which don't
	support EPSV mode. Can be overridden by the 'GIT_CURL_FTP_NO_EPSV'
	environment variable. Default is false (curl will use EPSV).

i18n.commitEncoding::
	Character encoding the commit messages are stored in; git itself
	does not care per se, but this information is necessary e.g. when
	importing commits from emails or in the gitk graphical history
	browser (and possibly at other places in the future or in other
	porcelains). See e.g. gitlink:git-mailinfo[1]. Defaults to 'utf-8'.

i18n.logOutputEncoding::
	Character encoding the commit messages are converted to when
	running `git-log` and friends.

log.showroot::
	If true, the initial commit will be shown as a big creation event.
	This is equivalent to a diff against an empty tree.
	Tools like gitlink:git-log[1] or gitlink:git-whatchanged[1], which
	normally hide the root commit will now show it. True by default.

merge.summary::
	Whether to include summaries of merged commits in newly created
	merge commit messages. False by default.

merge.tool::
	Controls which merge resolution program is used by
	gitlink:git-mergetool[1].  Valid values are: "kdiff3", "tkdiff",
	"meld", "xxdiff", "emerge", "vimdiff", "gvimdiff", and "opendiff".

merge.verbosity::
	Controls the amount of output shown by the recursive merge
	strategy.  Level 0 outputs nothing except a final error
	message if conflicts were detected. Level 1 outputs only
	conflicts, 2 outputs conflicts and file changes.  Level 5 and
	above outputs debugging information.  The default is level 2.
	Can be overridden by 'GIT_MERGE_VERBOSITY' environment variable.

merge.<driver>.name::
	Defines a human readable name for a custom low-level
	merge driver.  See gitlink:gitattributes[5] for details.

merge.<driver>.driver::
	Defines the command that implements a custom low-level
	merge driver.  See gitlink:gitattributes[5] for details.

merge.<driver>.recursive::
	Names a low-level merge driver to be used when
	performing an internal merge between common ancestors.
	See gitlink:gitattributes[5] for details.

pack.window::
	The size of the window used by gitlink:git-pack-objects[1] when no
	window size is given on the command line. Defaults to 10.

pack.depth::
	The maximum delta depth used by gitlink:git-pack-objects[1] when no
	maximum depth is given on the command line. Defaults to 50.

pack.windowMemory::
	The window memory size limit used by gitlink:git-pack-objects[1]
	when no limit is given on the command line.  The value can be
	suffixed with "k", "m", or "g".  Defaults to 0, meaning no
	limit.

pack.compression::
	An integer -1..9, indicating the compression level for objects
	in a pack file. -1 is the zlib default. 0 means no
	compression, and 1..9 are various speed/size tradeoffs, 9 being
	slowest.  If not set,  defaults to core.compression.  If that is
	not set,  defaults to -1, the zlib default, which is "a default
	compromise between speed and compression (currently equivalent
	to level 6)."

pack.deltaCacheSize::
	The maximum memory in bytes used for caching deltas in
	gitlink:git-pack-objects[1].
	A value of 0 means no limit. Defaults to 0.

pack.deltaCacheLimit::
	The maximum size of a delta, that is cached in
	gitlink:git-pack-objects[1]. Defaults to 1000.

pack.threads::
	Specifies the number of threads to spawn when searching for best
	delta matches.  This requires that gitlink:git-pack-objects[1]
	be compiled with pthreads otherwise this option is ignored with a
	warning. This is meant to reduce packing time on multiprocessor
	machines. The required amount of memory for the delta search window
	is however multiplied by the number of threads.

pack.indexVersion::
	Specify the default pack index version.  Valid values are 1 for
	legacy pack index used by Git versions prior to 1.5.2, and 2 for
	the new pack index with capabilities for packs larger than 4 GB
	as well as proper protection against the repacking of corrupted
	packs.  Version 2 is selected and this config option ignored
	whenever the corresponding pack is larger than 2 GB.  Otherwise
	the default is 1.

pull.octopus::
	The default merge strategy to use when pulling multiple branches
	at once.

pull.twohead::
	The default merge strategy to use when pulling a single branch.

remote.<name>.url::
	The URL of a remote repository.  See gitlink:git-fetch[1] or
	gitlink:git-push[1].

remote.<name>.proxy::
	For remotes that require curl (http, https and ftp), the URL to
	the proxy to use for that remote.  Set to the empty string to
	disable proxying for that remote.

remote.<name>.fetch::
	The default set of "refspec" for gitlink:git-fetch[1]. See
	gitlink:git-fetch[1].

remote.<name>.push::
	The default set of "refspec" for gitlink:git-push[1]. See
	gitlink:git-push[1].

remote.<name>.skipDefaultUpdate::
	If true, this remote will be skipped by default when updating
	using the remote subcommand of gitlink:git-remote[1].

remote.<name>.receivepack::
	The default program to execute on the remote side when pushing.  See
	option \--exec of gitlink:git-push[1].

remote.<name>.uploadpack::
	The default program to execute on the remote side when fetching.  See
	option \--exec of gitlink:git-fetch-pack[1].

remote.<name>.tagopt::
	Setting this value to --no-tags disables automatic tag following when fetching
	from remote <name>

remotes.<group>::
	The list of remotes which are fetched by "git remote update
	<group>".  See gitlink:git-remote[1].

repack.usedeltabaseoffset::
	Allow gitlink:git-repack[1] to create packs that uses
	delta-base offset.  Defaults to false.

show.difftree::
	The default gitlink:git-diff-tree[1] arguments to be used
	for gitlink:git-show[1].

showbranch.default::
	The default set of branches for gitlink:git-show-branch[1].
	See gitlink:git-show-branch[1].

status.relativePaths::
	By default, gitlink:git-status[1] shows paths relative to the
	current directory. Setting this variable to `false` shows paths
	relative to the repository root (this was the default for git
	prior to v1.5.4).

tar.umask::
	This variable can be used to restrict the permission bits of
	tar archive entries.  The default is 0002, which turns off the
	world write bit.  The special value "user" indicates that the
	archiving user's umask will be used instead.  See umask(2) and
	gitlink:git-archive[1].

user.email::
	Your email address to be recorded in any newly created commits.
	Can be overridden by the 'GIT_AUTHOR_EMAIL', 'GIT_COMMITTER_EMAIL', and
	'EMAIL' environment variables.  See gitlink:git-commit-tree[1].

user.name::
	Your full name to be recorded in any newly created commits.
	Can be overridden by the 'GIT_AUTHOR_NAME' and 'GIT_COMMITTER_NAME'
	environment variables.  See gitlink:git-commit-tree[1].

user.signingkey::
	If gitlink:git-tag[1] is not selecting the key you want it to
	automatically when creating a signed tag, you can override the
	default selection with this variable.  This option is passed
	unchanged to gpg's --local-user parameter, so you may specify a key
	using any method that gpg supports.

whatchanged.difftree::
	The default gitlink:git-diff-tree[1] arguments to be used
	for gitlink:git-whatchanged[1].

imap::
	The configuration variables in the 'imap' section are described
	in gitlink:git-imap-send[1].

receive.unpackLimit::
	If the number of objects received in a push is below this
	limit then the objects will be unpacked into loose object
	files. However if the number of received objects equals or
	exceeds this limit then the received pack will be stored as
	a pack, after adding any missing delta bases.  Storing the
	pack from a push can make the push operation complete faster,
	especially on slow filesystems.

receive.denyNonFastForwards::
	If set to true, git-receive-pack will deny a ref update which is
	not a fast forward. Use this to prevent such an update via a push,
	even if that push is forced. This configuration variable is
	set when initializing a shared repository.

transfer.unpackLimit::
	When `fetch.unpackLimit` or `receive.unpackLimit` are
	not set, the value of this variable is used instead.
